export const appHeaderHeight: number = 82

export const styles = theme => ({
  bodyContainer: {
    [theme.breakpoints.up('lg')]: {
      width: '80%',
      margin: 'auto'
    },
    [theme.breakpoints.down('md')]: {
      marginLeft: '30px',
      marginRight: '30px'
    },
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center'
  },
  navList: {
    paddingTop: 0,
    paddingBottom: 0
  },
  saveButton: {
    position: 'absolute',
    zIndex: 2,
    right: '15px',
    top: '15px'
  },
  [theme.breakpoints.up('md')]: {
    backgroundColor: theme.palette.primary.main
  },
  leftGrid: {
    [theme.breakpoints.up('md')]: {
      paddingRight: '5px'
    },
    [theme.breakpoints.down('sm')]: {
      paddingBottom: '50px'
    },
    paddingTop: 0,
    position: 'relative'
  },
  rightGrid: {
    paddingTop: 0,
    [theme.breakpoints.up('md')]: {
      paddingLeft: '10px'
    },
    [theme.breakpoints.down('sm')]: {
      paddingTop: '10px'
    },
    position: 'relative',
    overflowY: 'auto'
  },
  root: {
    width: '100%',
    maxWidth: 360,
    backgroundColor: theme.palette.background.paper,
  },
  paper: {
    width: '80%',
    maxHeight: 435,
  },
})
