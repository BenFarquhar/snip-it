export type Props = {
  foo: number,
  bar?: string,
  addSppet: Function,
  snippets: Array<any>,
  history: any
}

export type AppState = {
  editor: any,
  height: number,
  width: number
}
