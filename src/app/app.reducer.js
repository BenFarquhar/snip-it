import { createReducer } from '../helper/reducer.js'

//for some reason this is what makes proptypes be in the component
const initialState = {
  snippets: [],
  history: []
}

const addSnippetReducerFunction = (state: any, action): any => {
  return Object.assign({}, state, {
    snippets: [
      ...state.snippets,
      {
        text: action.payload.text,
        id: action.payload.id
      }
    ]
  })
}

const editSnippetReducerFunction = (state: any, action): any => ({
  ...state,
  snippets: state.snippets.map(snippet =>
    snippet.id === action.payload.id
      ? {...snippet, text: action.payload.text}
      : snippet
  )
})

const removeSnippetReducerFunction = (state: any, action): any => {
  const newSnippets = state.snippets.filter(snippet => snippet.id !== action.payload.id)
  return { snippets: newSnippets, history: [] }
}

export const rootReducer: any = createReducer(initialState, {
  ADD_SNIPPET: addSnippetReducerFunction,
  EDIT_SNIPPET: editSnippetReducerFunction,
  REMOVE_SNIPPET: removeSnippetReducerFunction
})
