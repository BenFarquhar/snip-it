import type { Action } from '../redux.model'
import type { Snippet } from '../snippet/snippet.model'
import * as types from './ActionTypes'

let nextSnippetId = 0
export const addSnippet = (payload: Snippet): Action => ({
  type: types.ADD_SNIPPET,
  payload: {
    text: payload.text,
    id: nextSnippetId++
  }
})

export const editSnippet = (payload: Snippet): Action => ({
  type: types.EDIT_SNIPPET,
  payload: {
    id: payload.snippetId,
    text: payload.snippetText
  }
})

export const removeSnippet = (payload: Snippet): Action => ({
  type: types.REMOVE_SNIPPET,
  payload: {
    id: payload.snippetId
  }
})
