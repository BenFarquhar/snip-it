import React, { Component } from 'react'
import './App.css'
import brace from 'brace'
import AceEditor from 'react-ace'
import 'brace/mode/javascript'
import 'brace/theme/gruvbox'
import Button from '@material-ui/core/Button'
import AddIcon from '@material-ui/icons/Add'
import { bindActionCreators, createStore } from 'redux'
import type { Dispatch } from 'redux'
import { addSnippet, editSnippet } from './action'
import { connect, Provider } from 'react-redux'
import { composeWithDevTools } from 'redux-devtools-extension'
import PropTypes from 'prop-types'
import Grid from '@material-ui/core/Grid'
import Snippet from '../snippet/Snippet'
import List from '@material-ui/core/List'
import { rootReducer } from './app.reducer.js'
import type { Props, AppState } from './app.model.js'
import { appHeaderHeight } from './app.style.js'
import { withStyles } from '@material-ui/core/styles'
import { styles } from './app.style.js'
import Header from '../header/Header'
import ConfirmDialog from '../confirm/ConfirmDialog'

// and so is this. proptypes needs it in initial state and also mapstatetoprops
const mapStateToProps = (
  state: any,
  ownProps: { buttonColour: string }
): any => ({
  snippets: state.snippets,
  history: state.history,
  buttonColour: ownProps.buttonColour
})

const mapDispatchToProps = (dispatch: Dispatch): any => {
  return bindActionCreators(
    {
      addSnippet: addSnippet,
      editSnippet: editSnippet
    },
    dispatch
  )
}

class AppComponent extends Component<Props, AppState> {
  constructor(props) {
    super(props)
    this.state = {
      width: 0,
      height: 0,
      editor: React.createRef(),
      saveButtonDisabled: true,
      editorValue: '',
      open: false,
      lastClickedSnippet: ''
    }
  }

  componentDidMount = () => {
    this.updateWindowDimensions()
    window.addEventListener('resize', this.updateWindowDimensions)
  }

  componentWillUnmount = () => {
    window.removeEventListener('resize', this.updateWindowDimensions)
  }

  updateWindowDimensions = () => {
    this.setState({
      width: window.innerWidth,
      height: window.innerHeight
    })
  }

  onEditorChange = editorValue => {
    if (editorValue.length > 5) {
      this.setState({ saveButtonDisabled: false })
    } else {
      this.setState({ saveButtonDisabled: true })
    }
    this.setState({ editorValue: editorValue })
  }

  onSaveButtonClick = () => {
    this.setState({ saveButtonDisabled: true })
    if (this.state.lastClickedSnippet) {
      this.props.editSnippet({
        snippetId: this.state.lastClickedSnippet.id,
        snippetText: this.state.editorValue
      })
      this.setState({ lastClickedSnippet: undefined })
    } else {
      this.props.addSnippet({
        text: this.state.editor.current.editor.getValue()
      })
    }
    this.setState({ editorValue: '' })
  }

  onSnippetClick = (snippet: Snippet) => {
    this.setState({ lastClickedSnippet: snippet })
    this.setState({ open: true })
  }

  onDialogClose = value => {
    this.setState({ value, open: false })
  }  
  
  handleOk = () => {
    this.setState({ editorValue: this.state.lastClickedSnippet.text })
    this.onDialogClose(this.state.value)
  };

  handleCancel = () => {
    this.setState({ lastClickedSnippet: undefined })
    this.onDialogClose(this.state.value)
  };

  render = () => {
    let allSnippets = []
    if (this.props.snippets) {
      allSnippets = this.props.snippets.map(snippet => (
        <Snippet
          snippet={snippet}
          key={snippet.id}
          onSnippetClick={this.onSnippetClick}
        />
      ))
    }

    return (
      <div className="App">
        <ConfirmDialog
          handleOk={this.handleOk}
          handleCancel={this.handleCancel}
          open={this.state.open}
          onDialogClose={this.onDialogClose}
          value={this.state.value}
        />
        <Header />
        <div
          className={this.props.classes.bodyContainer}
          style={{ height: this.state.height - appHeaderHeight - 70 }}>
          <Grid
            container
            spacing={0}
            alignItems={'flex-start'}
            direction={'row'}
            justify={'flex-start'}>
            <Grid
              item
              sm={12}
              md={6}
              className={this.props.classes.leftGrid}
              style={{ height: this.state.height - appHeaderHeight - 70 }}>
              <Button
                className={this.props.classes.saveButton}
                variant="fab"
                color="secondary"
                aria-label="Add"
                disabled={this.state.saveButtonDisabled}
                onClick={this.onSaveButtonClick}>
                <AddIcon />
              </Button>
              <AceEditor
                mode="javascript"
                theme="gruvbox"
                width="100%"
                value={this.state.editorValue}
                onChange={this.onEditorChange}
                height={this.state.height - appHeaderHeight - 70}
                name="editor"
                editorProps={{ $blockScrolling: true }}
                ref={this.state.editor}
              />
            </Grid>
            <Grid
              item
              sm={12}
              md={6}
              className={this.props.classes.rightGrid}
              style={{ height: this.state.height - appHeaderHeight - 70 }}>
              <List component="nav" className={this.props.classes.navList} />
              {allSnippets}
            </Grid>
          </Grid>
        </div>
      </div>
    )
  }
}

const App = withStyles(styles)(
  connect(
    mapStateToProps,
    mapDispatchToProps
  )(AppComponent)
)

AppComponent.propTypes = {
  snippets: PropTypes.arrayOf(
    PropTypes.shape({
      id: PropTypes.number.isRequired,
      text: PropTypes.string.isRequired
    }).isRequired
  ).isRequired,
  history: PropTypes.array,
  addSnippet: PropTypes.func.isRequired
}

const store = createStore(rootReducer, composeWithDevTools())

const SnippetApp = () => (
  <Provider store={store}>
    <App />
  </Provider>
)

export default SnippetApp
