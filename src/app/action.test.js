import * as actions from './action'
import * as types from './ActionTypes'

describe('actions', () => {
  it('should create an action to add a snippet', () => {
    const snippetText = 'Finish docs'
    const snippetId = 0
    const expectedAction = {
      type: types.ADD_SNIPPET,
      payload: {
        text: snippetText,
        id: snippetId
      }
    }
    expect(actions.addSnippet({ text: snippetText })).toEqual(expectedAction)
  })
  it('should create an action to edit a snippet', () => {
    const snippetText = 'Finish docs'
    const snippetId = 0
    const expectedAction = {
      type: types.EDIT_SNIPPET,
      payload: {
        text: snippetText,
        id: snippetId
      }
    }
    expect(actions.editSnippet({ text: snippetText, id: snippetId })).toEqual(expectedAction)
  })
  it('should create an action to add a snippet', () => {
    const snippetText = 'Finish docs'
    const snippetId = 0
    const expectedAction = {
      type: types.ADD_SNIPPET,
      payload: {
        text: snippetText,
        id: snippetId
      }
    }
    expect(actions.addSnippet({ text: snippetText })).toEqual(expectedAction)
  })
})
