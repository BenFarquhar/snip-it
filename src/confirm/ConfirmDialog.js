import React from 'react'
import PropTypes from 'prop-types'
import { withStyles } from '@material-ui/core/styles'
import Button from '@material-ui/core/Button'
import DialogTitle from '@material-ui/core/DialogTitle'
import DialogContent from '@material-ui/core/DialogContent'
import DialogActions from '@material-ui/core/DialogActions'
import Dialog from '@material-ui/core/Dialog'

class ConfirmDialog extends React.Component<any, any> {
  radioGroupRef = null;

  constructor(props) {
    super()
    this.state.value = props.value
  }

  state = {};

  // TODO
  componentWillReceiveProps(nextProps) {
    if (nextProps.value !== this.props.value) {
      this.setState({ value: nextProps.value })
    }
  }



  handleChange = (event, value) => {
    this.setState({ value })
  };

  render() {
    const { value, ...other } = this.props

    return (
      <Dialog
        maxWidth="xs"
        onEntering={this.handleEntering}
        aria-labelledby="confirmation-dialog-title"
        {...other}>
        <DialogTitle id="confirmation-dialog-title">Edit Snippet</DialogTitle>
        <DialogContent >
            Lose current editor text and load snippet?
        </DialogContent>
        <DialogActions>
          <Button onClick={this.props.handleCancel} color="primary">
            Cancel
          </Button>
          <Button onClick={this.props.handleOk} color="primary">
            Ok
          </Button>
        </DialogActions>
      </Dialog>
    )
  }
}

ConfirmDialog.propTypes = {
  onClose: PropTypes.func,
  value: PropTypes.string,
}

export default ConfirmDialog