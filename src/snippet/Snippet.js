import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { withStyles } from '@material-ui/core/styles'
import ListItem from '@material-ui/core/ListItem'
import ListItemIcon from '@material-ui/core/ListItemIcon'
import ListItemText from '@material-ui/core/ListItemText'
import ListItemSecondaryAction from '@material-ui/core/ListItemSecondaryAction'
import Button from '@material-ui/core/Button'
import MoreHorizIcon from '@material-ui/icons/MoreHoriz'
import CodeIcon from '@material-ui/icons/Code'
import { styles } from './snippet.style.js'
import Menu from '@material-ui/core/Menu'
import MenuItem from '@material-ui/core/MenuItem'
import { removeSnippet } from '../app/action'
import { editSnippet } from '../app/action'
import { bindActionCreators } from 'redux'
import type { Dispatch } from 'redux'
import { connect } from 'react-redux'

const mapDispatchToProps = (dispatch: Dispatch): any => {
  return bindActionCreators(
    {
      removeSnippet: removeSnippet,
      editSnippet: editSnippet
    },
    dispatch
  )
}

class SnippetComponent extends Component<any, any> {
  constructor(props) {
    super(props)
    this.state = {
      anchorEl: undefined
    }
  }

  handleClick = event => {
    this.setState({ anchorEl: event.currentTarget })
  }

  handleClose = () => {
    this.setState({ anchorEl: null })
  }

  handleRemove = () => {
    this.props.removeSnippet({snippetId: this.props.snippet.id})
  }
  render = () => {
    return (
      <ListItem
        button
        onClick={() => this.props.onSnippetClick(this.props.snippet)}
        className={this.props.classes.listItem}>
        <ListItemIcon>
          <CodeIcon />
        </ListItemIcon>
        <ListItemText
          style={{
            marginRight: '100px',
            whiteSpace: 'nowrap',
            overflow: 'hidden',
            textOverflow: 'ellipsis'
          }}
          primary={this.props.snippet.text}
        />
        <ListItemSecondaryAction>
          <Button
            variant="fab"
            color="primary"
            aria-owns={this.state.anchorEl ? 'simple-menu' : null}
            aria-haspopup="true"
            onClick={this.handleClick}
            className={this.props.classes.iconHover}
            style={{
              marginRight: '50px',
              boxShadow: 'none',
              color: 'white'
            }}
            aria-label="Add">
            <MoreHorizIcon />
          </Button>
          <Menu
            id="simple-menu"
            anchorEl={this.state.anchorEl}
            open={Boolean(this.state.anchorEl)}
            onClose={this.handleClose}>
            <MenuItem onClick={this.handleRemove}>Remove Snippet</MenuItem>
            <MenuItem onClick={this.handleClose}>Share</MenuItem>
          </Menu>
        </ListItemSecondaryAction>
      </ListItem>
    )
  }
}

SnippetComponent.propTypes = {
  classes: PropTypes.object.isRequired,
  snippet: PropTypes.object.isRequired
}

const Snippet = withStyles(styles)(
  connect(
    undefined,
    mapDispatchToProps
  )(SnippetComponent)
)

export default withStyles(styles)(Snippet)
