export const styles = theme => ({
  listItem: {
    height: '100px',
    backgroundColor: 'whitesmoke'
  },
  iconHover: {
    '&:hover': {
        background: 'rgba(63,81,181,1)'
      },
      background: 'rgba(63,81,181,0.5)'
  }
})
